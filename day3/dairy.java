import java.util.Scanner;
class dairy{
  static final float cart = 3.78f;
  final static float cost = 0.38f;
  final static float profit = 0.27f/cart;
  public static void main(String[] args){

    Scanner i = new Scanner(System.in);
    System.out.println("Enter amount of milk produced ( In litre)");
    float milk = i.nextFloat();
    double req_cart = milk/cart;
    float total_cost = milk*cost;
    float total_profit = milk*profit;


    System.out.println("Required Number of cartoon for " +(int) Math.ceil(milk)+" Litre of milk is: "+(int) Math.ceil(req_cart));
    System.out.println("The total cost is: "+total_cost);
    System.out.println("The total profit is: "+total_profit);

  }
}
